package br.com.silvamap.digitounico.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrivateKeyDTO {

    private Long userId;
    private String privateKey;
}
