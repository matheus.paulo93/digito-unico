package br.com.silvamap.digitounico.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalcResultDTO {

    private Long id;

    private Long userId;

    private String inputN;

    private Integer inputK;

    private Integer result;

}
