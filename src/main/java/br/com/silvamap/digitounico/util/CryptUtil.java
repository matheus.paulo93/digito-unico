package br.com.silvamap.digitounico.util;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class CryptUtil {

    private static String CIPHER_ALGORITHM = "RSA/None/OAEPWithSHA-1AndMGF1Padding";
    private static String KEYFACTORY_ALGORITHM = "RSA";

    public static String encrypt(String text, String publicKey) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {

        Security.addProvider(new BouncyCastleProvider());
        KeyFactory kf = KeyFactory.getInstance(KEYFACTORY_ALGORITHM);
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] textEncrypt = cipher.doFinal(text.getBytes());
        return Base64.getEncoder().encodeToString(textEncrypt);
    }

    public static String decrypt(String encryptText, String privateKey)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException,
            IllegalBlockSizeException, BadPaddingException {

        KeyFactory kf = KeyFactory.getInstance(KEYFACTORY_ALGORITHM);
        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
        RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);

        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privKey);
        byte[] textDecrypt = cipher.doFinal(Base64.getDecoder().decode(encryptText.getBytes()));
        return new String(textDecrypt);
    }
}
