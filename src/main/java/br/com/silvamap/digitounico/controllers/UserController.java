package br.com.silvamap.digitounico.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.silvamap.digitounico.dto.UserDTO;
import br.com.silvamap.digitounico.dto.UserSimpleDTO;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert-user")
    public ResponseEntity<UserDTO> insertUser(@RequestBody UserDTO userDTO) {
        userDTO = userService.insertUser(userDTO);
        return ResponseEntity.ok(userDTO);
    }

    @GetMapping("/find-user-by-id/{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable("id") Long id) throws GenericError {
        UserDTO userDTO = userService.findUserById(id);
        return ResponseEntity.ok(userDTO);
    }

    @GetMapping("/list-users")
    public ResponseEntity<List<UserSimpleDTO>> listUsers(@RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "page", required = false) Integer page) {
        List<UserSimpleDTO> listUsers = userService.listUsers(page, limit);
        return ResponseEntity.ok(listUsers);
    }

    @PutMapping("/update-user")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserSimpleDTO userSimpleDTO) throws GenericError {
        UserDTO userDTO = userService.updateUser(userSimpleDTO);
        return ResponseEntity.ok(userDTO);
    }

    @DeleteMapping("/delete-user-by-id/{id}")
    public ResponseEntity<String> deleteUserById(@PathVariable("id") Long id) {
        userService.deleteUserById(id);
        return ResponseEntity.ok("Usuário deletado com sucesso.");
    }
}
