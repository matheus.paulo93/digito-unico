package br.com.silvamap.digitounico.controllers;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.silvamap.digitounico.dto.PrivateKeyDTO;
import br.com.silvamap.digitounico.dto.PublicKeyDTO;
import br.com.silvamap.digitounico.dto.UserSimpleDTO;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.service.CryptService;

@RestController
@RequestMapping("/crypt")
public class CryptController {

    @Autowired
    private CryptService cryptService;

    @PostMapping("/encrypt-data")
    public ResponseEntity<String> encryptData(@RequestBody PublicKeyDTO publicKeyDTO) throws GenericError, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
        cryptService.encryptData(publicKeyDTO);
        return ResponseEntity.ok("Dados de usuário criptografados com sucesso.");
    }

    @PostMapping("/decrypt-data")
    public ResponseEntity<UserSimpleDTO> decryptData(@RequestBody PrivateKeyDTO privateKeyDTO)
            throws GenericError, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
        UserSimpleDTO userDTO = cryptService.decryptData(privateKeyDTO);
        return ResponseEntity.ok(userDTO);
    }

}
