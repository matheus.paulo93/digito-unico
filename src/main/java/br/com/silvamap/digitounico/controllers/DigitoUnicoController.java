package br.com.silvamap.digitounico.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.silvamap.digitounico.dto.CalcResultDTO;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.service.DigitoUnicoService;

@RestController
@RequestMapping("/digito-unico")
public class DigitoUnicoController {

    @Autowired
    private DigitoUnicoService digitoUnicoService;

    @PostMapping("/calcular-digito-unico")
    public ResponseEntity<Integer> calcularDigitoUnico(@RequestBody CalcResultDTO calcResultDTO)
            throws GenericError {
        Integer result = digitoUnicoService.calcularDigitoUnico(calcResultDTO);
        return ResponseEntity.ok(result);
    }
}
