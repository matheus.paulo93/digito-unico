package br.com.silvamap.digitounico.service;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.silvamap.digitounico.cache.Cache;
import br.com.silvamap.digitounico.dto.CalcResultDTO;
import br.com.silvamap.digitounico.entities.CalcResult;
import br.com.silvamap.digitounico.entities.User;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.repository.CalcResultRepository;
import br.com.silvamap.digitounico.repository.UserRepository;

@Service
public class DigitoUnicoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Cache cache;

    @Autowired
    private CalcResultRepository calcResultRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * 
     * @param n - string representando um inteiro.
     * @param k - inteiro representando o número de vezes da concatenação de "n"
     * @return
     * @throws Exception
     */
    public Integer digitoUnico(String n, Integer k) throws GenericError {
        try {
            Integer intN = Integer.parseInt(n);
            if (intN == 0) {
                throw new GenericError("\"n\" deve ser maior que zero.");
            }
            StringBuilder strN = new StringBuilder();
            for (int i = 0; i < k; i++) {
                strN.append(n);
            }
            return digitoUnico(strN.toString());
        } catch (NumberFormatException e) {
            throw new GenericError("\"n\" deve ser necessariamente um número.");
        }
    }

    /**
     * 
     * @param str - numeros que serão usando para calcular o digito único.
     * @return
     */
    public Integer digitoUnico(String str) {
        if (str.split("").length == 1) {
            return Integer.parseInt(str.split("")[0]);
        }
        Integer numero = 0;
        for (String strN : str.split("")) {
            numero += Integer.parseInt(strN);
        }
        return digitoUnico(numero.toString());
    }

    public Integer calcularDigitoUnico(CalcResultDTO calcResultDTO) throws GenericError {
        Integer result = cache.getResultCache(calcResultDTO);
        if (result == null) {
            result = digitoUnico(calcResultDTO.getInputN(), calcResultDTO.getInputK());
            cache.addCache(calcResultDTO);
        }
        calcResultDTO.setResult(result);
        CalcResult calcResult = modelMapper.map(calcResultDTO, CalcResult.class);
        if (calcResultDTO.getUserId() != null) {
            Optional<User> user = userRepository.findById(calcResultDTO.getUserId());
            if (user.isPresent()) {
                calcResult.setUser(user.get());
            } else {
                throw new GenericError("Usuário informado não encontrado.");
            }
        }
        calcResultRepository.save(calcResult);
        return result;
    }

}
