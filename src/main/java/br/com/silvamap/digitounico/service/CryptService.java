package br.com.silvamap.digitounico.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.silvamap.digitounico.dto.PrivateKeyDTO;
import br.com.silvamap.digitounico.dto.PublicKeyDTO;
import br.com.silvamap.digitounico.dto.UserDTO;
import br.com.silvamap.digitounico.dto.UserSimpleDTO;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.util.CryptUtil;

@Service
public class CryptService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserService userService;

    public void encryptData(PublicKeyDTO publicKeyDTO) throws GenericError, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
        if (publicKeyDTO.getUserId() == null) {
            throw new GenericError("É obrigatório informar o ID do usuário.");
        }
        if (publicKeyDTO.getPublicKey() == null) {
            throw new GenericError("É obrigatório informar a chave pública do usuário.");
        }
        UserDTO userDTO = userService.findUserById(publicKeyDTO.getUserId());
        if (userDTO.getPublicKey() != null) {
            throw new GenericError("Os dados deste usuário já estão criptografados.");
        }
        userDTO.setName(CryptUtil.encrypt(userDTO.getName(), publicKeyDTO.getPublicKey()));
        userDTO.setEmail(CryptUtil.encrypt(userDTO.getEmail(), publicKeyDTO.getPublicKey()));
        userDTO.setPublicKey(publicKeyDTO.getPublicKey());
        userService.updateUser(modelMapper.map(userDTO, UserSimpleDTO.class));
    }

    public UserSimpleDTO decryptData(PrivateKeyDTO privateKeyDTO)
            throws GenericError, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
        UserDTO userDTO = userService.findUserById(privateKeyDTO.getUserId());
        if (userDTO.getPublicKey() != null) {
            userDTO.setName(CryptUtil.decrypt(userDTO.getName(), privateKeyDTO.getPrivateKey()));
            userDTO.setEmail(CryptUtil.decrypt(userDTO.getEmail(), privateKeyDTO.getPrivateKey()));
            return modelMapper.map(userDTO, UserSimpleDTO.class);
        } else {
            throw new GenericError("Os dados deste usuário não foram criptografados previamente.");
        }
    }

}
