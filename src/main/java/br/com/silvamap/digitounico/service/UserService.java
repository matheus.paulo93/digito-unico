package br.com.silvamap.digitounico.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.silvamap.digitounico.dto.UserDTO;
import br.com.silvamap.digitounico.dto.UserSimpleDTO;
import br.com.silvamap.digitounico.entities.User;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    public UserDTO insertUser(UserDTO userDTO) {
        User user = modelMapper.map(userDTO, User.class);
        user = userRepository.save(user);
        userDTO.setId(user.getId());
        return userDTO;
    }

    public UserDTO findUserById(Long id) throws GenericError {
        User user = userRepository.findById(id).orElseThrow(() -> new GenericError("Usuário não encontrado."));
        return modelMapper.map(user, UserDTO.class);
    }

    public List<UserSimpleDTO> listUsers(Integer page, Integer limit) {
        if (page == null) {
            page = 0;
        }
        if (limit == null) {
            limit = 10;
        }
        Pageable pageable = PageRequest.of(page, limit);
        List<User> listUser = userRepository.findAll(pageable).getContent();
        return listUser.stream().map(u -> modelMapper.map(u, UserSimpleDTO.class)).collect(Collectors.toList());
    }

    public UserDTO updateUser(UserSimpleDTO userSimpleDTO) throws GenericError {
        if (userSimpleDTO.getId() == null) {
            throw new GenericError("É obrigatório informar o id do usuário.");
        }
        User user = userRepository.findById(userSimpleDTO.getId())
                .orElseThrow(() -> new NoResultException("Usuário não encontrado."));

        user = modelMapper.map(userSimpleDTO, User.class);
        user = userRepository.save(user);
        return modelMapper.map(user, UserDTO.class);
    }

    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

}
