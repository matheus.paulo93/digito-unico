package br.com.silvamap.digitounico.exceptions;

public class GenericError extends Exception {

    private static final long serialVersionUID = 1L;

    public GenericError(String message) {
        super(message);
    }
}
