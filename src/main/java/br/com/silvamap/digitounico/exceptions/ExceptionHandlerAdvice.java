package br.com.silvamap.digitounico.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(GenericError.class)
    public ResponseEntity<String> erro(GenericError exception) {
        String msg = exception.getMessage();
        return ResponseEntity.ok(msg);
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<String> erro(Throwable exception) {
        String msg = exception.getMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
    }

}
