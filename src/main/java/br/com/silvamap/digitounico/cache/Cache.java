package br.com.silvamap.digitounico.cache;

import java.util.LinkedList;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.silvamap.digitounico.dto.CalcResultDTO;
import lombok.Data;

@Component
@Scope("application")
@Data
public class Cache {

    private LinkedList<CalcResultDTO> listResults;

    @PostConstruct
    public void init() {
        listResults = new LinkedList<>();
    }

    public void addCache(CalcResultDTO calcResultDTO) {
        if (listResults.size() == 10) {
            listResults.removeFirst();
        }
        listResults.add(calcResultDTO);
    }

    public Integer getResultCache(CalcResultDTO calcResultDTO) {
        for (CalcResultDTO calcDto : listResults) {
            if (calcResultDTO.getInputK().equals(calcDto.getInputK())
                    && calcResultDTO.getInputN().equals(calcDto.getInputN())) {
                return calcDto.getResult();
            }
        }
        return null;
    }
}
