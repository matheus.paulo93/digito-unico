package br.com.silvamap.digitounico.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "name", length = 1048)
    private String name;

    @Column(name = "email", length = 1048)
    private String email;

    @Column(name = "public_key", length = 1048)
    private String publicKey;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CalcResult> listResults;

}
