package br.com.silvamap.digitounico.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.silvamap.digitounico.entities.CalcResult;

public interface CalcResultRepository extends JpaRepository<CalcResult, Long> {

}
