package br.com.silvamap.digitounico.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.silvamap.digitounico.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
