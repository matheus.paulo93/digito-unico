package br.com.silvamap.digitounico;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import br.com.silvamap.digitounico.dto.UserDTO;
import br.com.silvamap.digitounico.dto.UserSimpleDTO;
import br.com.silvamap.digitounico.service.UserService;

public class UserControllerTest extends DigitoUnicoApplicationTests {

    private final String BASE_PATH = "http://localhost:8080/user";

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserService userService;

    private RestTemplate restTemplate;

    private Long idMatheus;

    @Before
    public void setUp() throws Exception {
        UserDTO userMatheus = userService
                .insertUser(new UserDTO(null, "Matheus Silva", "matheus.silva@email.com", null, null));
        idMatheus = userMatheus.getId();
        restTemplate = new RestTemplate();
    }

    @Test
    public void testInsertUser() throws Exception {
        UserDTO userDTO = new UserDTO(null, "Leandro Costa", "leandro.costa@gmail.com", null, null);
        UserDTO userDTOResponse = restTemplate.postForObject(BASE_PATH + "/insert-user", userDTO, UserDTO.class);

        Assert.assertEquals("Leandro Costa", userDTOResponse.getName());
        Assert.assertNotNull(userDTOResponse.getId());
    }

    @Test
    public void testFindUserById() throws Exception {
        UserDTO userDTO = restTemplate.getForObject(BASE_PATH + "//find-user-by-id/" + idMatheus, UserDTO.class);
        Assert.assertNotNull(userDTO);
        Assert.assertEquals("Matheus Silva", userDTO.getName());
    }

    @Test
    public void testUpdateUser() throws Exception {
        UserDTO userDTO = restTemplate.getForObject(BASE_PATH + "//find-user-by-id/" + idMatheus, UserDTO.class);
        UserSimpleDTO userSimpleDTO = modelMapper.map(userDTO, UserSimpleDTO.class);
        userSimpleDTO.setName("NOME MODIFICADO");
        userSimpleDTO.setEmail("EMAIL MODIFICADO");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<UserSimpleDTO> entity = new HttpEntity<UserSimpleDTO>(userSimpleDTO, headers);
        ResponseEntity<UserDTO> userDTOResponse = restTemplate.exchange(BASE_PATH + "/update-user", HttpMethod.PUT,
                entity, UserDTO.class);
        Assert.assertEquals("NOME MODIFICADO", userDTOResponse.getBody().getName());
        Assert.assertEquals("EMAIL MODIFICADO", userDTOResponse.getBody().getEmail());
    }

    @Test
    public void testDeleteUserById() throws Exception {
        restTemplate.delete(BASE_PATH + "/delete-user-by-id/" + idMatheus);
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(BASE_PATH + "//find-user-by-id/" + idMatheus,
                String.class);
        Assert.assertTrue(responseEntity.getBody().contains("Usuário não encontrado"));
    }

}
