package br.com.silvamap.digitounico;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.silvamap.digitounico.dto.CalcResultDTO;
import br.com.silvamap.digitounico.exceptions.GenericError;
import br.com.silvamap.digitounico.service.DigitoUnicoService;

public class DigitoUnicoServiceTest extends DigitoUnicoApplicationTests {

    @Autowired
    private DigitoUnicoService digitoUnicoService;

    @Test
    public void calcularDigitoUnicoTest() throws Exception, GenericError {
        Integer result = digitoUnicoService.calcularDigitoUnico(new CalcResultDTO(null, null, "9875", 4, 8));
        Assert.assertTrue(result.equals(8));
    }

    @Test
    public void digitoUnicoTest1() throws Exception, GenericError {
        Integer result = digitoUnicoService.digitoUnico("9875", 4);
        Assert.assertTrue(result.equals(8));
    }

    @Test
    public void digitoUnicoTest2() throws Exception, GenericError {
        Integer result = digitoUnicoService.digitoUnico("9875");
        Assert.assertTrue(result.equals(2));
    }

}
