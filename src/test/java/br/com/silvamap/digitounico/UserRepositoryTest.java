package br.com.silvamap.digitounico;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.silvamap.digitounico.entities.User;
import br.com.silvamap.digitounico.repository.UserRepository;

public class UserRepositoryTest extends DigitoUnicoApplicationTests {

    @Autowired
    private UserRepository userRepository;

    private Long idMatheus;

    @Before
    public void setUp() throws Exception {
        User user = userRepository.save(new User(null, "Matheus Silva", "matheus.silva@gmail.com", null, null));
        idMatheus = user.getId();
    }

    @Test
    public void testFindUserById() throws Exception {
        Optional<User> user = userRepository.findById(idMatheus);
        Assert.assertTrue(user.isPresent());
        Assert.assertEquals("Matheus Silva", user.get().getName());
    }
}
