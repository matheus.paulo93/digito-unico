package br.com.silvamap.digitounico;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.silvamap.digitounico.cache.Cache;
import br.com.silvamap.digitounico.dto.CalcResultDTO;

public class CacheTest extends DigitoUnicoApplicationTests {

    @Autowired
    private Cache cache;

    @Before
    public void setUp() {
        cache.getListResults().add(new CalcResultDTO(null, null, "9875", 4, 8));
        cache.getListResults().add(new CalcResultDTO(null, null, "4567", 5, 2));
        cache.getListResults().add(new CalcResultDTO(null, null, "321", 6, 9));
        cache.getListResults().add(new CalcResultDTO(null, null, "42685", 3, 3));
        cache.getListResults().add(new CalcResultDTO(null, null, "12345", 4, 6));
        cache.getListResults().add(new CalcResultDTO(null, null, "32165", 8, 1));
        cache.getListResults().add(new CalcResultDTO(null, null, "6547", 3, 3));
        cache.getListResults().add(new CalcResultDTO(null, null, "2587", 5, 2));
        cache.getListResults().add(new CalcResultDTO(null, null, "7418", 3, 6));
        cache.getListResults().add(new CalcResultDTO(null, null, "96325", 4, 1));
    }

    @Test
    public void testAddCache() throws Exception {
        cache.addCache(new CalcResultDTO());
        Assert.assertTrue(cache.getListResults().size() == 10);
    }

    @Test
    public void testGetResultCache() throws Exception {
        CalcResultDTO calcResultDTO = new CalcResultDTO(null, null, "4567", 5, 2);
        Integer result = cache.getResultCache(calcResultDTO);
        Assert.assertEquals(calcResultDTO.getResult(), result);
    }

}
