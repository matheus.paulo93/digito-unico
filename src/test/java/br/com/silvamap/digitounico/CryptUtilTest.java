package br.com.silvamap.digitounico;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.silvamap.digitounico.util.CryptUtil;

public class CryptUtilTest {

    private static String TEXT = "digito-unico";
    private String encryptText;
    private static String PUBLIC_KEY = "MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBYEsKv9JVBKfbd1YS7yHZqT9f8lSBCoJ2bHPv53tRjk3MlgxG8BYzBzpbRaF3H63EItgNn/OKvwaDovls2wxBTsgwZKQQXlWzo32Ij4bRhMQWc2qXx6qH8n9g2xgKYTpnSlEtlL3C6xMIqzOpUpPLrF7uD96TNz8IjBXSdvcExmBF3Nlnt8CgNiK/NhSX7jVzszF93elY9bHboAt2POKv4r2QDd9+FLuR9Nteg1No4drh7DP8M/Qox2dyqXZpL1vscyjKm0WNIS2tW/wBMKq6aAIUub9x2Is2ldHZ2g4WiCKK3un/6+7KRDnfz75umwZLDfTt6E4CemynftbLy2asPAgMBAAE=";
    private static String PRIVATE_KEY = "MIIEogIBAAKCAQBYEsKv9JVBKfbd1YS7yHZqT9f8lSBCoJ2bHPv53tRjk3MlgxG8BYzBzpbRaF3H63EItgNn/OKvwaDovls2wxBTsgwZKQQXlWzo32Ij4bRhMQWc2qXx6qH8n9g2xgKYTpnSlEtlL3C6xMIqzOpUpPLrF7uD96TNz8IjBXSdvcExmBF3Nlnt8CgNiK/NhSX7jVzszF93elY9bHboAt2POKv4r2QDd9+FLuR9Nteg1No4drh7DP8M/Qox2dyqXZpL1vscyjKm0WNIS2tW/wBMKq6aAIUub9x2Is2ldHZ2g4WiCKK3un/6+7KRDnfz75umwZLDfTt6E4CemynftbLy2asPAgMBAAECggEATVsc+KLgPT34gFs7nBu0Nigu0sDAdwTPqR/KCWu4LEWkCzyV5QkS2l7zhLYgOKoIu9fyIfGxwSFWjVY0k2K9AmYNzkOCPpyV7evWLSHe5sbB4UUCH/svj81tgJ5mRRQ0cwqhNhx27PRIfoNBBltiqS5NGtn2PHbQUyZFiQ7TkxSvLmxEdw4sPDvoaPVnuPu4faohxw9Ey8wbR+BUm8nMKjqO5o4a4sCLzX4g1LTx5Xc+IBW2QywlN3OM3EEx26uI0HzaeBbmOm6rQSAOIByzU48PIxWTQRW13MNB6omAGa8rIFD0rLQ9CtxP1cOnuNTPYFROz2PubJugPopbC1IHIQKBgQCh3jFXlT/W8iOYiWGfKGVdo70DCWVZveZx6a2Yik+TcEl1viWaZzITzj66q4zY+hmdMBoRpKBx1lKeHQ0+TFLWnIgICOvVONjF+X3NklhNlBxWWkP8qfh6tZ1nIU+CZfdfFuBNuyZROZKy22ZBu1ldgLioMEU5NuxZkWlfvQckRwKBgQCLSoRTI/m/Bs/Q8wKxbdIcnTgsNMuK/w1yP3QNXTmKS+/yFzBNq4qPPpxl2NGs23LtYYrW4dgvoei6wBgE/FoznztCh9TkxPfR1MA4iIzQyxqrSuCtKmanJDUZ5E37hIJ0ivrRXoHpuFvw/brhKYRL+ROC4PpPDVauP0EjJPiO+QKBgEOPlBS0TNMQed7+6Gof2D8/SnLsBwtwAbFmEXjrsc8ePOsSFGvfyr0Nho0vcThXGz3Kh/b5OaaKCC3Kfm6UOLcu2nBQGO8ELSZkvieBf63Rtgos3teksfu1th8HfkI1AQ0FZVXMWW/pkdtpS8L/yYsHYQuCKkJ5NCkmUk8NfN+dAoGBAIT46kIEghMfCZ1LoDKJyJ+QZ2OsMvPKcKe0c7GeYaPownO2N2KudGQzPVLtHZ2LEbgT+lsQ1zUBuA8v+r4numS1ifkFevO6JWqnU6mkkelxO1By9vHZHI5LPnYtQVTvNyrS0oa1Vggs+3iggJsa6zUr3rh+1fZJoYpO0HwgezjhAoGAT3eSR3I0f9svlXBX4eV/HXF2yIWuI74L9PH2fqo5EXP+7HCafOuMYw45IvYEsOSW4hgM/1zcqSWEdz/Kv6TDFWRmt4O51xKTudOICsH/zHETB5yoQ6VdkIruVy8Bq5i6IJ+bET47gHkHf3QU7gOhHX2z0PKGovFALoLY/XeoStQ=";

    @Before
    public void setUp() throws Exception {
        encryptText = CryptUtil.encrypt(TEXT, PUBLIC_KEY);
    }

    @Test
    public void testEncrypt() throws Exception {
        String encryptText = CryptUtil.encrypt(TEXT, PUBLIC_KEY);
        String decryptText = CryptUtil.decrypt(encryptText, PRIVATE_KEY);
        Assert.assertEquals(decryptText, TEXT);
    }

    @Test
    public void testDecrypt() throws Exception {
        String decryptText = CryptUtil.decrypt(encryptText, PRIVATE_KEY);
        Assert.assertEquals(TEXT, decryptText);
    }
}
