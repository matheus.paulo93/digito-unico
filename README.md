# Projeto digito-unico

Projeto desenvolvido para o Desafio Java do Banco Inter

## Compilando e executando

Após clonar o repositorio, entre na pasta raíz e execute o seguinte comando para compilar a aplicação:

```bash
mvn install -DskipTests
```
E para executar a aplicação:

```bash
 java -jar target/digito-unico-0.0.1-SNAPSHOT.jar
```

Acesse http://localhost:8080/swagger-ui.html para ter acesso à documentação da API.

## Executando testes unitários

Para executar os testes unitários use o seguinte comando:

```bash
mvn test
```

Abra o arquivo "\digito-unico\target\site\jacoco\index.hml" em sem navegador, para ter acesso ao relatório de cobertura de código pelos testes unitários.

## Lombok

Este projeto utiliza a framework Project Lombok, se for utilizar alguma IDE, para evitar erros de compilação é necessário instalar o Lombok em sua IDE, acesse o site https://projectlombok.org/ para saber como instalar na IDE que você estiver utilizando.

## Executando testes no Postman

Para testar as APIs REST, importe o arquivo "postman_collection.json" no Postman.